"""
All the configs/args handling.
"""

import argparse
from typing import Dict, List
import socket
import yaml

from pinger.dataclass import Host, PingerConfig
from pinger import DEFAULT_PING_TIMEOUT, DEFAULT_BULK_SIZE, DEFAULT_PINGS_PER_HOST


class ConfigError(Exception):
    """This resprensent an error in the config file."""


def parse_args():
    """Parse arguments over the cmdline with argparse."""
    parser = argparse.ArgumentParser(description="Pinger")
    parser.add_argument(
        "-c",
        "--carbon-host",
        type=str,
        help="Ship data to this Carbon Server",
        default="filer-carbon.cern.ch",
    )
    parser.add_argument(
        "-p",
        "--carbon-port",
        type=str,
        help="Ship data to this port on the Carbon Server",
        default=2004,
    )
    parser.add_argument(
        "-f",
        "--config-file",
        type=str,
        help="Location of the config file",
        required=True,
    )
    parser.add_argument(
        "--ssh-config",
        type=str,
        help="SSH config for the SSH probe",
    )
    parser.add_argument(
        "-t",
        "--timeout",
        type=float,
        default=DEFAULT_PING_TIMEOUT,
        help="Timeout of the ping in seconds",
    )
    parser.add_argument(
        "-m",
        "--max-bulk-size",
        type=int,
        default=DEFAULT_BULK_SIZE,
        help="Maximum number of host which will be pinged in one bulk",
    )
    parser.add_argument(
        "--pings-per-host",
        type=int,
        default=DEFAULT_PINGS_PER_HOST,
        help="Number of ping sent to one host each time",
    )
    parser.add_argument(
        "--ip-family", type=str, default="ipv4", choices=["ipv4", "ipv6"]
    )

    return parser.parse_args()


def parse_host(data, address_required=True) -> Host:
    """Parse an host node in yaml config."""
    if "address" in data:
        address = data["address"]
    else:
        if address_required:
            raise ConfigError("An address for a host has not been provided!")
        address = None

    if "name" in data:
        name = data["name"]
    else:
        name = address

    name = name.replace(".", "_")
    return Host(name=name, address=address)


def parse_current_host(data):
    """Parse the host node for the machine the script runs."""
    address = None
    if "current_host" in data:
        host = parse_host(data, address_required=False)
        if host.name is not None:
            return host
        address = host.address

    name = socket.gethostname().replace(".", "_")
    return Host(name=name, address=address)


def parse_config(config_path: str) -> PingerConfig:
    """Parse the yaml config and the args and return a PingerConfig."""
    hosts: List[Host] = list()
    group_dict: Dict[Host, str] = dict()

    with open(config_path) as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)

    current_host = parse_current_host(config)

    for group in config["groups"]:
        group_name = group["name"]
        group_content = group["content"]

        for host_config in group_content:
            host = parse_host(host_config)
            if host in group_dict and group_dict[host] != group_name:
                raise ConfigError("A host is in two group in the config!")

            hosts.append(host)
            group_dict[host] = group_name

    return PingerConfig(
        current_host=current_host,
        group_dict=group_dict,
        hosts=hosts,
    )
