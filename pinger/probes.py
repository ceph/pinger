"""
Where all the probes are implemented.
"""

import time
import socket

import asyncio
import asyncssh  # type: ignore
import icmplib  # type: ignore

from pinger import SSH_USERNAME
from pinger.logic import Pinger


class PingerICMP(Pinger):
    """The Pinger implementation for ICMP"""

    def __init__(self):
        super().__init__("icmp")

    async def simple_ping(
        self, host: str, timeout: float, family: socket.AddressFamily
    ) -> float:
        resp = icmplib.ping(host, timeout=timeout, count=1)

        if not resp.is_alive:
            raise asyncio.TimeoutError("Timeout while ping")

        return resp.min_rtt


class PingerSSH(Pinger):
    """The Pinger implementation for SSH"""

    def __init__(self, ssh_config: str = None):
        super().__init__("ssh")
        self.ssh_config = ssh_config

    async def simple_ping(
        self, host: str, timeout: float, family: socket.AddressFamily
    ) -> float:
        start_time = time.perf_counter()

        try:
            await asyncio.wait_for(
                asyncssh.connect(
                    host,
                    username=SSH_USERNAME,
                    config=self.ssh_config,
                    known_hosts=None,
                    family=family,
                ),
                timeout,
            )
        except asyncssh.PermissionDenied:  # Ignore permissionDenied
            pass

        return (time.perf_counter() - start_time) * 1000
