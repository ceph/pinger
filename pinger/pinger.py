#!/usr/bin/env python3

"""
The main pinger module.
"""

import logging
import socket

import asyncio

from pinger import probes
from pinger import carbon
from pinger import configs


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


PINGER_SSH = probes.PingerSSH()
PINGER_ICMP = probes.PingerICMP()

PINGERS = [
    PINGER_SSH,
    PINGER_ICMP,
]


async def main_async():
    """
    The actual main function.
    """
    logger.info("Starting pinger")
    args = configs.parse_args()
    config = configs.parse_config(args.config_file)

    PINGER_SSH.ssh_config = args.ssh_config

    ip_family = None
    if args.ip_family == "ipv4":
        ip_family = socket.AF_INET
    elif args.ip_family == "ipv6":
        ip_family = socket.AF_INET6

    for pinger in PINGERS:
        logger.info("Starting probe %s", pinger.protocol)
        ping_results = await pinger.ping(
            config.hosts,
            args.timeout / len(PINGERS),
            args.max_bulk_size,
            args.pings_per_host,
            family=ip_family,
        )

        if ping_results:
            logger.info("Sending %s data to carbon", ping_results[0].protocol)
        await carbon.send_results_to_carbon(
            args.carbon_host, args.carbon_port, config, ping_results, args.ip_family
        )

    logger.info("Pinger finished")


def main():
    """The main function called by the pinger executable."""
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main_async())


if __name__ == "__main__":
    main()
