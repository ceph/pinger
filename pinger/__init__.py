"""The pinger package gather information with probe and send latency stat to a
carbon server.
"""
# Default variables
DEFAULT_PING_TIMEOUT = 120  # 2 mins
DEFAULT_BULK_SIZE = 1
DEFAULT_PINGS_PER_HOST = 5
SSH_USERNAME = "pinger-monitoring"
